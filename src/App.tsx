import React, {useEffect, useState} from 'react';
import {LinkContainer} from "react-router-bootstrap";
import {Navbar, Nav} from "react-bootstrap";
import './App.css';
import Routes from "./Routes"
import {AppContext} from "./libs/contextLib";
import {Auth} from "aws-amplify";
import {useHistory} from "react-router";

function App() {
    const [isAuthenticated, userHasAuthenticated] = useState(false);
    const [isAuthenticating, setIsAuthenticating] = useState(true);
    const history = useHistory();
    useEffect(() => onLoad(), []);
    const onLoad = () => {
        Auth.currentSession()
            .then(() => userHasAuthenticated(true))
            .catch(e => {
                if (e !== "No current user") {
                    alert(e);
                }
            })
            .finally(() => setIsAuthenticating(false));
    };
    function handleLogout():void {
        Auth.signOut()
            .then(() => {
                userHasAuthenticated(false);
                history.push("/login");
            });

    }
    return (
        isAuthenticating
        ? <></>
        :
        <div className="App container">
            <Navbar expand={"lg"} bg={"light"}>
                <LinkContainer to={"/"}>
                    <Navbar.Brand>Fresstreat</Navbar.Brand>
                </LinkContainer>

                <Nav className="mr-auto"></Nav>
                <Navbar.Toggle id="justify-content-end"/>
                <Navbar.Collapse id="justify-content-end">
                    <Nav >
                        {isAuthenticated
                        ? <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
                        : <>
                                <LinkContainer to={"/signup"}>
                                    <Nav.Link>Signup</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to={"/login"}>
                                    <Nav.Link>Login</Nav.Link>
                                </LinkContainer>
                            </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <AppContext.Provider value={{isAuthenticated, userHasAuthenticated}}>
                <Routes/>
            </AppContext.Provider>
        </div>
    );
}

export default App;
