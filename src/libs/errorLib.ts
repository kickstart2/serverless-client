const onError = (error:Error) => {
    let message:string = error.toString();
    //Auth errors
    if (error.message) {
        message = error.message;
    }

    alert(message);
};

export {onError}
