export type Recipe = {
    recipeId:string
    content:string,
    attachment:string,
    attachmentUrl:string,
    createdAt:number
}
