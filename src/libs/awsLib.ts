import {Storage} from "aws-amplify";

const s3Upload = (file:File | null):Promise<string> => {
    if (!file) {
        return Promise.resolve("");
    }

    const filename = `${Date.now()}-${file.name}`;

    return Storage.vault.put(filename, file, {
        contentType: file.type
    })
        .then((result:any) => {
            return result.key;
        })
        .catch((reason:any) => {
            return Promise.reject(reason);
        })
};

const s3Delete = (attachment:string):Promise<void> => {
    if (!attachment) {
        return Promise.resolve();
    }

    return Storage.vault.remove(attachment)
        .catch(reason => {
            return Promise.reject(reason);
        })
};

export {s3Upload, s3Delete}
