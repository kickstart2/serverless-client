import {useContext, createContext, Context} from "react";

type ContextType = {
    isAuthenticated:boolean,
    userHasAuthenticated:React.Dispatch<React.SetStateAction<boolean>>,
}

export const AppContext = createContext<ContextType | undefined>(undefined);
export function useAppContext() {
    return useContext(AppContext as Context<ContextType>);
}
