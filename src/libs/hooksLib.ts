import {ChangeEvent, useState} from "react";

type FormControlElement = HTMLInputElement
    | HTMLSelectElement
    | HTMLTextAreaElement;

const useFormFields:<S>(initialState:S) => [S, (event:ChangeEvent<FormControlElement>) => void] = <S>(initialState:S) => {
    const [fields, setValues] = useState(initialState);

    return [
        fields,
        (event:ChangeEvent<FormControlElement>) => {
            setValues({
                    ...fields,
                    [event.target.id]: event.target.value
                });
        }
    ];
};

export {useFormFields}
