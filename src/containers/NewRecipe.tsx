import React, {ChangeEvent, FormEvent, useRef, useState} from "react";
import {useHistory} from "react-router";
import {Form} from "react-bootstrap";
import config from "../config"
import LoaderButton from "../components/LoaderButton";
import {API} from "aws-amplify";
import "./NewRecipe.css";
import {onError} from "../libs/errorLib";
import {s3Upload} from "../libs/awsLib";

function NewRecipe() {
    const file = useRef<File | null>(null);
    const history = useHistory();
    const [content, setContent] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    function validateForm() {
        return content.length > 0;
    }

    async function handleSubmit(event:FormEvent<HTMLFormElement>) {
        event.preventDefault();

        if (file.current && file.current.size > config.MAX_ATTACHMENT_SIZE) {
            alert(`Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE / 1000000} MB.`);
            return;
        }

        setIsLoading(true);

        s3Upload(file.current)
            .then((attachment:string) => createRecipe({content, attachment}))
            .then(() => history.push("/"))
            .catch((reason) => onError(reason))
            .finally(() => setIsLoading(false));
    }

    function createRecipe(recipe:{content:string, attachment:string}):Promise<any> {
        return API.post("recipes", "/recipes", {
            body: recipe
        });
    }

    function handleFileChange(event:ChangeEvent<HTMLInputElement>) {
        file.current = (event.target as HTMLInputElement).files![0];
    }

    return (
        <div className={"NewRecipe"}>
            <form onSubmit={handleSubmit}>
                <Form.Group controlId={"content"}>
                    <Form.Control
                        value={content}
                        as={"textarea"}
                        onChange={e => setContent(e.target.value)}
                    />
                </Form.Group>
                <Form.Group controlId={"file"}>
                    <Form.Label>Attachment</Form.Label>
                    <Form.Control onChange={handleFileChange} type={"file"}/>
                </Form.Group>
                <LoaderButton
                    block
                    type={"submit"}
                    bsPrefix={"large"}
                    variant={"primary"}
                    disabled={!validateForm()}
                    isLoading={isLoading}>
                    Create
                </LoaderButton>
            </form>
        </div>
    )
}

export default NewRecipe
