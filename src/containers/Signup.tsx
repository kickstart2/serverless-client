import React from "react";
import {useFormFields} from "../libs/hooksLib";
import {useHistory} from "react-router";
import {FormEvent, useState} from "react";
import {useAppContext} from "../libs/contextLib";
import {Form} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import {Auth} from "aws-amplify";
import "./Signup.css"
import {onError} from "../libs/errorLib";

function Signup() {
    const [fields, handleFieldChange] = useFormFields({
        email: "",
        password: "",
        confirmPassword: "",
        confirmationCode: ""
    });
    const history = useHistory();
    const [newUser, setNewUser] = useState<string>();
    const {userHasAuthenticated} = useAppContext();
    const [isLoading, setIsLoading] = useState(false);

    function validateForm() {
        return (
            fields.email.length > 0 &&
            fields.password.length > 0 &&
            fields.password === fields.confirmPassword
        );
    }

    function validateConfirmationForm() {
        return fields.confirmationCode.length > 0;
    }

    function handleSubmit(event:FormEvent):Promise<void> {
        event.preventDefault();
        setIsLoading(true);
        return Auth.signUp({
            username: fields.email,
            password: fields.password
        })
            .then((newUser) => {
                setNewUser(newUser.user.getUsername());
            })
            .catch(e => {
                onError(e);
            })
            .finally(() => {
                setIsLoading(false);
            });
    }

    function handleConfirmationSubmit(event:FormEvent):Promise<void> {
        event.preventDefault();
        setIsLoading(true);
        return Auth.confirmSignUp(fields.email, fields.confirmationCode)
            .then(() => Auth.signIn(fields.email, fields.password))
            .then(() => {
                userHasAuthenticated(true);
                history.push("/");
            })
            .catch((e) => {
                onError(e);
                setIsLoading(false);
            });
    }

    function renderConfirmationForm() {
        return (
            <form onSubmit={handleConfirmationSubmit}>
                <Form.Group controlId={"confirmationCode"} bsPrefix={"large"}>
                    <Form.Label>Confirmation Code</Form.Label>
                    <Form.Control
                        autoFocus
                        type={"tel"}
                        onChange={handleFieldChange}
                        value={fields.confirmationCode}
                    />
                    <Form.Text id={"confirmationCodeHelpBlock"} muted>Please check your email for the code.</Form.Text>
                </Form.Group>
                <LoaderButton
                    block
                    type={"submit"}
                    bsPrefix={"large"}
                    isLoading={isLoading}
                    disabled={!validateConfirmationForm()}
                >
                    Verify
                </LoaderButton>
            </form>
        );
    }

    function renderForm() {
        return (
            <form onSubmit={handleSubmit}>
                <Form.Group controlId={"email"} bsPrefix={"large"}>
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        autoFocus
                        type={"email"}
                        value={fields.email}
                        onChange={handleFieldChange}
                    />
                </Form.Group>
                <Form.Group controlId={"password"} bsPrefix={"large"}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type={"password"}
                        value={fields.password}
                        onChange={handleFieldChange}
                    />
                </Form.Group>
                <Form.Group controlId={"confirmPassword"} bsPrefix={"large"}>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type={"password"}
                        value={fields.confirmPassword}
                        onChange={handleFieldChange}
                    />
                </Form.Group>
                <LoaderButton
                    block
                    type={"submit"}
                    bsPrefix={"large"}
                    isLoading={isLoading}
                    disabled={!validateForm()}
                >
                    Signup
                </LoaderButton>
            </form>
        )
    }

    return (
        <div className={"Signup"}>
            {newUser === undefined ? renderForm() : renderConfirmationForm()}
        </div>
    );
}

export default Signup;
