import React, {FormEvent, useState} from "react";
import {Form} from "react-bootstrap";
import {Auth} from "aws-amplify";
import {useAppContext} from "../libs/contextLib";
import "./Login.css"
import LoaderButton from "../components/LoaderButton";
import {onError} from "../libs/errorLib";
import {useFormFields} from "../libs/hooksLib";

function Login() {
    const {userHasAuthenticated} = useAppContext();
    const [isLoading, setIsLoading] = useState(false);
    const [fields, handleFieldChange] = useFormFields({
        email: "",
        password: ""
    });

    function validateForm():boolean {
        return fields.email.length > 0 && fields.password.length > 0;
    }

    function handleSubmit(event:FormEvent<HTMLFormElement>):void {
        event.preventDefault();
        setIsLoading(true);
        Auth.signIn(fields.email, fields.password)
            .then(response => {
                userHasAuthenticated(true);
            })
            .catch(e => {
                onError(e);
                setIsLoading(false);
            });
    }

    return (
        <div className={"Login"}>
            <form onSubmit={handleSubmit}>
                <Form.Group controlId={"email"} bsPrefix={"large"}>
                    <Form.Label column={true}>Email</Form.Label>
                    <Form.Control
                        autoFocus
                        type={"email"}
                        value={fields.email}
                        onChange={handleFieldChange}/>
                </Form.Group>
                <Form.Group controlId={"password"} bsPrefix={"large"}>
                    <Form.Label column={true}>Password</Form.Label>
                    <Form.Control
                        autoFocus
                        type={"password"}
                        value={fields.password}
                        onChange={handleFieldChange}/>
                </Form.Group>
                <LoaderButton block bsPrefix={"large"} disabled={!validateForm()} type={"submit"} isLoading={isLoading}>Login</LoaderButton>
            </form>

        </div>
    )
}

export default Login
