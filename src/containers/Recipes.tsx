import React, {ChangeEvent, FormEvent, MouseEvent, useEffect, useRef, useState} from "react";
import {useHistory, useParams} from "react-router";
import {Recipe} from "../libs/modelLib";
import {API, Storage} from "aws-amplify";
import {onError} from "../libs/errorLib";
import config from "../config";
import {Button, Form} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import "./Recipes.css";
import {s3Delete, s3Upload} from "../libs/awsLib";

function Recipes() {
    const file = useRef<File | null>(null);
    const {id} = useParams();
    const history = useHistory();
    const [recipe, setRecipe] = useState<Recipe | null>(null);
    const [content, setContent] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [isDeleting, setIsDeleting] = useState(false);

    useEffect(() => {
        function loadRecipe():Promise<Recipe> {
            return API.get("recipes", `/recipes/${id}`, {});
        }
        let recipeReference:Recipe;
        loadRecipe()
            .then((loadedRecipe) => {
                recipeReference = loadedRecipe;
            })
            .then(() => {
                if (recipeReference.attachment) {
                    return Storage.vault.get(recipeReference.attachment) as Promise<string>
                }
                return Promise.resolve("");
            })
            .then((attachmentUrl:string) => {
                recipeReference.attachmentUrl = attachmentUrl.toString();
                setContent(recipeReference.content);
                setRecipe(recipeReference);
            })
            .catch(reason => onError(reason));
    }, [id]);

    function validateForm() {
        return content.length > 0;
    }

    function isDisabled():boolean {
        return isLoading || isDeleting;
    }

    function formatFilename(name:string) {
        return name.replace(/^\w+-/, "");
    }

    function handleFileChange(event:ChangeEvent<HTMLInputElement>) {
        file.current = (event.target as HTMLInputElement).files![0];
    }

    function handleSubmit(event:FormEvent<HTMLFormElement>) {

        event.preventDefault();

        if (file.current && file.current.size > config.MAX_ATTACHMENT_SIZE) {
            alert(`Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE / 1000000} MB.`);
            return;
        }

        setIsLoading(true);

        const attachmentToDelele = file.current ? recipe!.attachment : "";

        s3Delete(attachmentToDelele)
            .then(() => s3Upload(file.current))
            .then((generatedAttachment:string) => saveRecipe({
                content,
                attachment: generatedAttachment || recipe!.attachment
            }))
            .then(recipe => history.push("/"))
            .catch(reason => {
                onError(reason);
                setIsLoading(false);
            })
    }

    function handleDelete(event:MouseEvent<HTMLButtonElement>) {
        event.preventDefault();

        const confirmed = window.confirm("Are you sure you want to delete this recipe?");
        if (!confirmed) {
            return;
        }

        setIsDeleting( true);

        s3Delete(recipe!.attachment)
            .then(() => deleteRecipe())
            .then(() => history.push("/"))
            .catch(reason => {
                onError(reason);
                setIsDeleting(false);
            });
    }

    function saveRecipe(recipe:{content:string, attachment:string}):Promise<Recipe> {
        return API.put("recipes", `/recipes/${id}`, {
            body: recipe
        })
    }

    function deleteRecipe():Promise<void> {
        return API.del("recipes", `/recipes/${id}`, {});
    }


    return (
        <div className={"Recipes"}>
            {recipe && (
                <form onSubmit={handleSubmit}>
                    <Form.Group controlId={"content"}>
                        <Form.Control
                            value={content}
                            as={"textarea"}
                            onChange={e => setContent(e.target.value)}
                            disabled={isDisabled()}
                        />
                    </Form.Group>
                    {recipe.attachment && (
                        <Form.Group>
                            <Form.Label>Attachment</Form.Label>
                            <Form.Text >
                                <a
                                target={"_blank"}
                                rel={"noopener noreferrer"}
                                href={recipe.attachmentUrl}
                                >
                                    {formatFilename(recipe.attachment)}
                                </a>
                            </Form.Text>
                        </Form.Group>
                    )}
                    <Form.Group controlId={"file"}>
                        {!recipe.attachment && <Form.Label>Attachment</Form.Label>}
                        <Form.Control onChange={handleFileChange} type={"file"} disabled={isDisabled()}/>
                    </Form.Group>
                    <LoaderButton
                        block
                        type={"submit"}
                        size={"sm"}
                        disabled={isDisabled() || !validateForm()}
                        isLoading={isLoading}
                    >
                        Save
                    </LoaderButton>
                    <LoaderButton
                        block
                        onClick={handleDelete}
                        isLoading={isDeleting}
                        variant={"danger"}
                        size={"sm"}
                        disabled={isDisabled()}
                        type={"button"}
                    >
                        Delete
                    </LoaderButton>
                </form>
            )}
        </div>
    )
}

export default Recipes
