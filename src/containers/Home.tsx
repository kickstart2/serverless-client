import React, {useEffect, useState} from "react";
import {ListGroup, ListGroupItem} from "react-bootstrap";
import {API} from "aws-amplify";
import "./Home.css";
import {useAppContext} from "../libs/contextLib";
import {Recipe} from "../libs/modelLib";
import {onError} from "../libs/errorLib";
import {LinkContainer} from "react-router-bootstrap";

function Home() {
    const [recipes, setRecipes] = useState<Recipe[]>([]);
    const {isAuthenticated} = useAppContext();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        loadRecipes()
            .then((recipes) => setRecipes(recipes))
            .catch(reason => onError(reason))
            .finally(() => setIsLoading(false));
    }, [isAuthenticated]);

    function loadRecipes():Promise<Recipe[]> {
        return API.get("recipes", "/recipes", {});
    }

    function renderRecipesList(recipes:Recipe[]) {
        const list = [(
            <LinkContainer key={"new"} to={"/recipes/new"}>
                <ListGroup.Item>
                    <h4>
                        <b>{"\uFF0B"}</b>Create a new recipe
                    </h4>
                </ListGroup.Item>
            </LinkContainer>
        )];
        recipes.map((recipe, i) =>
            list.push((
                <LinkContainer key={recipe.recipeId} to={`/recipes/${recipe.recipeId}`}>
                    <ListGroup.Item>
                        <p>{recipe.content.trim().split("\n")[0]}</p>
                        {"Created: " + new Date(recipe.createdAt).toLocaleString()}
                    </ListGroup.Item>
                </LinkContainer>
            ))
        );

        return list;
    }

    function renderLander() {
        return (
            <div className={"lander"}>
                <h1>Fresstreat</h1>
                <p>A simple recipe taking app</p>
            </div>
        )
    }

    function renderRecipes() {
        return (
            <div className={"recipes"}>
                <div className={"pb-2 mt-4 mb-2 border-bottom"}>
                    Your Recipes
                </div>
                <ListGroup>
                    {!isLoading && renderRecipesList(recipes)}
                </ListGroup>
            </div>
        )
    }

    return (
        <div className={"Home"}>
            {isAuthenticated ? renderRecipes() : renderLander()}
        </div>
    )
}

export default Home;
