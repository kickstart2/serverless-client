import React from "react";
import "./NotFound.css";

function NotFound() {
    return (
        <div className={"NotFound"}>
            <h3>Page not found</h3>
        </div>
    )
}

export default NotFound;
