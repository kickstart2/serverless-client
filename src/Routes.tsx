import React from "react";
import {Route, Switch} from "react-router-dom";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import Login from "./containers/Login";
import Signup from "./containers/Signup";
import NewRecipe from "./containers/NewRecipe";
import Recipes from "./containers/Recipes";
import UnauthenticatedRoute from "./components/UnauthenticatedRoute";
import AuthenticatedRoute from "./components/AuthenticatedRoute";

function Routes() {
    return (
        <Switch>
            <Route exact path={"/"}>
                <Home/>
            </Route>
            <UnauthenticatedRoute exact path={"/login"}>
                <Login/>
            </UnauthenticatedRoute>
            <UnauthenticatedRoute exact path={"/signup"}>
                <Signup/>
            </UnauthenticatedRoute>
            <AuthenticatedRoute exact path={"/recipes/new"}>
                <NewRecipe/>
            </AuthenticatedRoute>
            <AuthenticatedRoute exact path={"/recipes/:id"}>
                <Recipes/>
            </AuthenticatedRoute>
            <NotFound/>
        </Switch>
    )
}

export default Routes;
