const dev = {
    s3: {
        REGION: "eu-central-1",
        BUCKET: "recipes-api-dev-attachmentsbucket-7png07c7g2ym"
    },
    apiGateway: {
        REGION: "eu-central-1",
        URL: "https://zkdqfyfkpf.execute-api.eu-central-1.amazonaws.com/dev"
    },
    cognito: {
        REGION: "eu-central-1",
        USER_POOL_ID: "eu-central-1_FDRzb8gsO",
        APP_CLIENT_ID: "p5jlgpkk2bgofigfbgeesiv3u",
        IDENTITY_POOL_ID: "eu-central-1:1305f5d2-9be0-4661-819d-1cee9a9da9f5"
    }
};

const prod = {
    s3: {
        REGION: "eu-central-1",
        BUCKET: "recipes-api-prod-attachmentsbucket-mih71bsq1ftx"
    },
    apiGateway: {
        REGION: "eu-central-1",
        URL: "https://710rvfl40b.execute-api.eu-central-1.amazonaws.com/prod"
    },
    cognito: {
        REGION: "eu-central-1",
        USER_POOL_ID: "eu-central-1_nbpT7xljj",
        APP_CLIENT_ID: "44a9qntb68eah6fmfcvvtklijh",
        IDENTITY_POOL_ID: "eu-central-1:fdf340d1-4ee7-4783-9593-830570600654"
    }
};

const config = process.env.REACT_APP_STAGE === "prod" ? prod : dev;

export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    ...config
};
