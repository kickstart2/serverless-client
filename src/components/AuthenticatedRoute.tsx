import React from "react";
import {Redirect, Route, RouteProps, useLocation} from "react-router";
import {useAppContext} from "../libs/contextLib";

function AuthenticatedRoute({children, ...rest}:RouteProps) {
    const {pathname, search} = useLocation();
    const {isAuthenticated} = useAppContext();

    return (
        <Route {...rest}>
            {isAuthenticated ? (
                children
            ) : (
                <Redirect to={`/login?redirect=${pathname}${search}`} />
            )}

        </Route>
    )
}

export default AuthenticatedRoute;
