import React, {FunctionComponent, ReactNode, ButtonHTMLAttributes} from "react";
import {Button, ButtonProps, Spinner} from "react-bootstrap";
import {BsPrefixProps} from "react-bootstrap/helpers";
import "./LoaderButton.css";


type ButtonParams =
    & ButtonProps
    & BsPrefixProps<FunctionComponent<any>>
    & Readonly<{ children?: ReactNode }>
    & ButtonHTMLAttributes<HTMLButtonElement>
    & {
        isLoading:boolean,
        className?:string,
        disabled:boolean,
    }

function LoaderButton({isLoading, className = "", disabled = false, ...props}:ButtonParams) {
    return (
        <Button className={`LoaderButton ${className}`} disabled={disabled || isLoading} {...props}>
            {isLoading && <Spinner as={"span"} animation={"border"} size={"sm"} role={"status"} aria-hidden={true}/>}
            {props.children}
        </Button>
    );
}

export default LoaderButton;
