import React from "react";
import {Redirect, Route, RouteProps, useLocation} from "react-router";
import {useAppContext} from "../libs/contextLib";

function querystring(name:string, url = window.location.href):string | null {
    name = name.replace(/[[]]/g, "\\$&");

    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i");
    const results = regex.exec(url);

    if (!results) {
        return null;
    }
    if (!results[2]) {
        return "";
    }

    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function UnauthenticatedRoute({children, ...rest}:RouteProps) {
    const {isAuthenticated} = useAppContext();
    const redirect = querystring("redirect");
    return (
        <Route {...rest}>
            {isAuthenticated ? (
                <Redirect to={redirect === "" || redirect === null ? "/" : redirect} />
            ) : (
                children
            )}

        </Route>
    )
}

export default UnauthenticatedRoute;
